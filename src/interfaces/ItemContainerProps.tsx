import IMovementProps from "./MovementProps";
import IDebuggingProps from "./DebuggingProps";
import IMinMaxProps from "./MinMaxProps";

export default interface IItemContainerProps {
    debug?: IDebuggingProps;
    swing?: IMovementProps;
    float?: IMovementProps;
    bounce?: IMovementProps;
    flip?: boolean;
    scale?: IMinMaxProps;
}