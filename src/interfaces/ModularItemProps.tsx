export default interface IModularItemProps {
    numTiles: number;
    numBushes: number;
    numTrees: number;
    numRocks: number;
    numVines: number;
}