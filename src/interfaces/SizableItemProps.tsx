export default interface ISizeableItemProps {
    width: number;
    height: number;
}