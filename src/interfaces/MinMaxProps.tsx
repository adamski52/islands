export default interface IMinMaxProps {
    min: number;
    max: number;
}
