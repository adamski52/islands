export type TAnimationTypes = "y" | "angle" | "scale";
export enum AnimationTypes {
    Y = "y",
    ANGLE = "angle",
    SCALE = "scale"
}