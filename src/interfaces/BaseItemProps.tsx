import IItemContainerProps from "./ItemContainerProps";
import ISizeableItemProps from "./SizableItemProps";

export default interface IBaseItemProps extends ISizeableItemProps, IItemContainerProps {
    img: string;
}