export default interface IRangeProps {
    atLeast: number;
    range: number;
}
