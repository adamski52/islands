import { EasingTypes } from "./EasingTypes";
import IRangeProps from "./RangeProps";
import IMinMaxProps from "./MinMaxProps";

export default interface IMovementProps {
    amount: IRangeProps;
    durationPerAmount: number;
    easeType: EasingTypes;
    delay?: IMinMaxProps;
    repeat?: boolean;
    loop?: boolean;
    reverse?: boolean;
}