import ISizeableItemProps from "./SizableItemProps";
import IItemContainerProps from "./ItemContainerProps";

export default interface IVariantProps extends ISizeableItemProps, IItemContainerProps {
    variants: string[];
}