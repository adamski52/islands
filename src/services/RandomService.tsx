export default class RandomService {
    public static getRandomFromSet(items:any[]) {
        return items[Math.floor(Math.random() * items.length)];
    }

    public static getRandomBetween(min:number, max:number) {
        // return Math.floor(Math.random() * (max - min + 1) + min);
        return Math.random() * (max - min) + min;
    }
}