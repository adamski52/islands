import IMovementProps from "../interfaces/MovementProps";
import {AnimationTypes} from "../interfaces/AnimationTypes";
import BaseAnimation from "./BaseAnimation";
import ItemContainer from "../components/items/ItemContainer";

export default class SwingAnimation extends BaseAnimation {
    protected property = AnimationTypes.ANGLE;

    constructor(target:ItemContainer, props:IMovementProps) {
        super(target, AnimationTypes.ANGLE, props);
    }
}