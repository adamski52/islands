import IMovementProps from "../interfaces/MovementProps";
import {AnimationTypes} from "../interfaces/AnimationTypes";
import { EasingTypes } from "../interfaces/EasingTypes";
import RandomService from "../services/RandomService";
import ItemContainer from "../components/items/ItemContainer";
import { ease } from "pixi-ease";

export default abstract class BaseAnimation {
    protected target:ItemContainer;
    protected property:AnimationTypes;
    protected props:IMovementProps;
    protected amount:number = 0;
    protected duration:number = 0;
    protected delay:number = 0;

    constructor(target:ItemContainer, property:AnimationTypes, props?:IMovementProps) {
        this.target = target;
        this.property = property;

        this.props = props || {
            amount: {
                atLeast: 0,
                range: 0
            },
            easeType: EasingTypes.LINEAR,
            durationPerAmount: 0
        };

        this.amount = this.figureAmount();
        this.duration = this.figureDuration();
        this.delay = this.figureDelay();
    }

    protected getOptions() {
        return {
            duration: this.duration,
            ease: this.props.easeType,
            repeat: !!!this.props.repeat,
            reverse: !!!this.props.reverse,
            wait: this.delay
        }
    }

    protected getProperty() {
        return {
            [this.property]: this.amount
        };
    }

    private figureDelay() {
        if(!this.props.delay) {
            return 0;
        }

        return RandomService.getRandomBetween(this.props.delay.min, this.props.delay.max);
    }

    protected figureAmount() {
        let absRange = Math.abs(this.props.amount.range),
            amount = RandomService.getRandomBetween(-absRange, absRange);
    
        return this.getAmountAdjustedForMinimum(amount);
    }

    protected getAmountAdjustedForMinimum(amount:number) {
        let absAmount = Math.abs(amount),
            absMin = Math.abs(this.props.amount.atLeast);

        if(absAmount < absMin) {
            if(amount < 0) {
                return -absMin;
            }

            return absMin;
        }

        return amount;
    }

    private figureDuration() {
        return Math.abs(this.props.durationPerAmount * this.amount);
    }

    public hookup() {
        let property = this.getProperty(),
            options = this.getOptions();

        ease.add(this.target, property, options);

        // console.log("HOOKED UP", this.target, property, options);
    }
}