import IMovementProps from "../interfaces/MovementProps";
import {AnimationTypes} from "../interfaces/AnimationTypes";
import BaseAnimation from "./BaseAnimation";
import ItemContainer from "../components/items/ItemContainer";

export default class BounceAnimation extends BaseAnimation {

    constructor(target:ItemContainer, props?:IMovementProps) {
        super(target, AnimationTypes.SCALE, props);
    }

    // protected getProperty() {
    //     let val = {
    //         scale: {
    //             x: this.amount,
    //             y: this.amount
    //         }
    //     };

    //     console.log(val);

    //     return val;
    // }
}