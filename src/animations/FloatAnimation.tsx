import IMovementProps from "../interfaces/MovementProps";
import {AnimationTypes} from "../interfaces/AnimationTypes";
import BaseAnimation from "./BaseAnimation";
import RandomService from "../services/RandomService";
import ItemContainer from "../components/items/ItemContainer";

export default class FloatAnimation extends BaseAnimation {

    constructor(target:ItemContainer, props?:IMovementProps) {
        super(target, AnimationTypes.Y, props);
    }

    protected figureAmount() {
        let absRange = Math.abs(this.props.amount.range),
            baseValue = this.target[this.property] as number,
            amount = RandomService.getRandomBetween(baseValue - absRange, baseValue + absRange);

        return this.getAmountAdjustedForMinimum(amount);
    }
}