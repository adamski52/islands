import React from 'react';
import * as PIXI from "pixi.js";
import ModularIsland from './items/ModularIsland';
import RandomService from '../services/RandomService';
import "./Stage.scss";

export default class Stage extends React.Component<any, any> {
    private app: PIXI.Application;
    private islands:ModularIsland[] = [];
    private ticker:PIXI.Ticker = PIXI.Ticker.shared;

    constructor(props: any) {
        super(props);
        this.ticker.autoStart = false;
        this.ticker.stop();

        this.app = new PIXI.Application({
            width: window.innerWidth,
            height: window.innerHeight,
            backgroundColor: 0x000000,
            // resolution: window.devicePixelRatio || 1
            resolution: 1
        });

        this.randomize = this.randomize.bind(this);
        this.onTick = this.onTick.bind(this);

        this.randomize();
    }

    private createIsland() {
        let numTiles = RandomService.getRandomBetween(0, 4),
            numBushes = RandomService.getRandomBetween(2, 10),
            numTilesMinus1 = numTiles - 1,
            numTrees = RandomService.getRandomBetween(1, numTilesMinus1 > 1 ? numTilesMinus1 : 1),
            numRocks = RandomService.getRandomBetween(0, numTilesMinus1 > 1 ? numTilesMinus1 : 1),
            numVines = RandomService.getRandomBetween(0, 2),
            island = new ModularIsland({
                numTiles,
                numBushes,
                numTrees,
                numRocks,
                numVines
            });

        island.scale.x = .5;
        island.scale.y = .5;

        this.islands.push(island);
        if(this.islands.length > 1) {
            let prevIsland = this.islands[this.islands.length - 2];
            island.x = prevIsland.x + prevIsland.width + 100;
            island.y = 400;
        }
        else {
            island.x = 100;
            island.y = 400;
        }

        this.app.stage.addChild(island);
    }

    private onTick(time:number) {
        this.app.render();
    }

    private randomize() {
        this.ticker.remove(this.onTick);
        this.app.stage.removeChildren();
        this.islands = [];

        this.createIsland();
        this.createIsland();
        this.createIsland();

        this.ticker.add(this.onTick);
    }

    public render() {
        return (
            <div className="wrapper">
                <div id="stage" />
                <button onClick={this.randomize}>Randomize</button>
            </div>
        );
    }

    public componentDidMount() {
        document.getElementById("stage")?.appendChild(this.app.view);
        this.ticker.start();
    }
}