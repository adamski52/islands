import IVariantProps from "../../interfaces/VariantProps";
import RandomService from "../../services/RandomService";
import BaseItem from "./BaseItem";

export default abstract class BaseVariantItem extends BaseItem {
    constructor(props:IVariantProps) {
        let img = RandomService.getRandomFromSet(props.variants);

        super({
            ...props,
            img: img,
            width: props.width,
            height: props.height
        });
    }
}