import IModularIslandProps from "../../interfaces/ModularItemProps";
import BaseItem from "./BaseItem";
import IslandTileLeftEdge from "./tiles/IslandTileLeftEdge";
import IslandTileRightEdge from "./tiles/IslandTileRightEdge";
import IslandTileCenter from "./tiles/IslandTileCenter";
import RandomService from "../../services/RandomService";
import ModularTree from "./ModularTree";
import Bush1 from "./floral/Bush1";
import Bush2 from "./floral/Bush2";
import Bush3 from "./floral/Bush3";
import Bush4 from "./floral/Bush4";
import Bush5 from "./floral/Bush5";
import Bush6 from "./floral/Bush6";
import Bush7 from "./floral/Bush7";
import Bush8 from "./floral/Bush8";
import Rock1 from "./floral/Rock1";
import Rock2 from "./floral/Rock2";
import Rock3 from "./floral/Rock3";
import Rock4 from "./floral/Rock4";
import Rock5 from "./floral/Rock5";
import Root1 from "./floral/Root1";
import Root2 from "./floral/Root2";
import Root3 from "./floral/Root3";
import Root4 from "./floral/Root4";
import Root5 from "./floral/Root5";
import Vine1 from "./floral/Vine1";
import Vine2 from "./floral/Vine2";
import ItemContainer from "./ItemContainer";
import { EasingTypes } from "../../interfaces/EasingTypes";


export default class ModularIsland extends ItemContainer {
    private tiles:BaseItem[] = [];

    constructor(props:IModularIslandProps) {
        super({
            float: {
                durationPerAmount: 10,
                amount: {
                    range: 40,
                    atLeast: 20
                },
                easeType: EasingTypes.EASE_IN_OUT_SINE
            }
        });

        this.addTiles(props.numTiles);
        this.addTrees(props.numTrees);
        this.addRocks(props.numRocks);
        this.addBushes(props.numBushes);
        this.addVines(props.numVines);
    }

    private getMinX(item:ItemContainer) {
        return item.getBounds().width * .75;
    }

    private getMaxX(item:ItemContainer) {
        return this.width - (item.getBounds().width * .75);
    }

    private addVines(num: number) {
        for(let i = 0; i < num; i++) {
            let container = new ItemContainer(),
                item = new (RandomService.getRandomFromSet([Vine1, Vine2]))();

            container.x = RandomService.getRandomBetween(this.getMinX(item) + 50, this.getMaxX(item) - 50);
            container.y = 50;

            container.addChild(item);
            this.addChildAt(container, 0);
        }
    }

    private addRocks(num: number) {
        for(let i = 0; i < num; i++) {
            let item = new (RandomService.getRandomFromSet([Rock1, Rock2, Rock3, Rock4, Rock5]))();

            item.x = RandomService.getRandomBetween(this.getMinX(item), this.getMaxX(item));
            item.y = 70;

            this.addChild(item);
        }
    }

    private addBushes(num: number) {
        for(let i = 0; i < num; i++) {
            let container = new ItemContainer(),
                item = new (RandomService.getRandomFromSet([Bush1, Bush2, Bush3, Bush4, Bush5, Bush6, Bush7, Bush8]))();

            item.x = RandomService.getRandomBetween(this.getMinX(item), this.getMaxX(item));
            item.y = 70;

            container.addChild(item);
            this.addChild(item);
        }
    }

    private addTrees(num:number) {
        for(let i = 0; i < num; i++) {
            let item = new ModularTree(),
                x = RandomService.getRandomBetween(this.getMinX(item), this.getMaxX(item));

            item.x = x;
            item.y = 70;
            this.addChildAt(item, 0);
            // this.addChild(item);

            item = new (RandomService.getRandomFromSet([Root1, Root2, Root3, Root4, Root5]))();
            item.x = x;
            item.y = 110;
            this.addChildAt(item, 0);
            // this.addChild(item);
        }
    }

    private addTiles(numTiles:number) {
        this.addTile(IslandTileLeftEdge);

        for(let i = 0; i < numTiles; i++) {
            this.addTile(IslandTileCenter);
        }

        this.addTile(IslandTileRightEdge);
    }

    private addTile(type: typeof BaseItem) {
        let tile;
        switch(type) {
            case IslandTileLeftEdge:
                tile = new IslandTileLeftEdge();
                break;
            case IslandTileRightEdge:
                tile = new IslandTileRightEdge();
                break;
            default:
                tile = new IslandTileCenter();
                break;
        } 
        
        this.tiles.push(tile);

        if(this.tiles.length > 1) {
            let prevItem = this.tiles[this.tiles.length - 2],
                x = prevItem.x + prevItem.getDisplaySize().width;

            tile.x = x;
            tile.y = 0;
        }

        this.addChild(tile);
    }
}