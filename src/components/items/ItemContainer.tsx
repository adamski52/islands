import * as PIXI from "pixi.js";
import IItemContainerProps from "../../interfaces/ItemContainerProps";
import RandomService from "../../services/RandomService";
import SwingAnimation from "../../animations/SwingAnimation";
import FloatAnimation from "../../animations/FloatAnimation";
import BounceAnimation from "../../animations/BounceAnimation";

export default class ItemContainer extends PIXI.Container {
    private indicator:PIXI.Graphics | undefined = undefined;
    private props:IItemContainerProps;

    constructor(props:IItemContainerProps = {}) {
        super();

        this.props = props;

        // this.hookupDebug();

        this.hookupScale();

        this.hookupFlip();

        setTimeout(() => {
            this.hookupSwing();
            this.hookupFloat();
            this.hookupBounce();
        });
    }

    private hookupScale() {
        if(!this.props.scale) {
            return;
        }
        
        let scale = RandomService.getRandomBetween(this.props.scale.min, this.props.scale.max);

        this.scale.x = scale;
        this.scale.y = scale;
    }

    private hookupFlip() {
        if(!this.props.flip) {
            return;
        }
        
        this.scale.x *= RandomService.getRandomFromSet([-1, 1]);
    }

    private hookupDebug() {
        if(!this.props.debug) {
            return;
        }

        this.indicator = new PIXI.Graphics();
        this.indicator.beginFill(this.props.debug.indicatorColor || 0xff0000);
        this.indicator.lineStyle(2, 0x000000);
        this.indicator.drawCircle(0, 0, this.props.debug.indicatorSize || 20);
        this.indicator.endFill();
        super.addChild(this.indicator);
    }

    private hookupSwing() {
        if(!this.props.swing) {
            return;
        }

        new SwingAnimation(this, this.props.swing).hookup();
    }

    private hookupFloat() {
        if(!this.props.float) {
            return;
        }

        new FloatAnimation(this, this.props.float).hookup();
    }

    private hookupBounce() {
        if(!this.props.bounce) {
            return;
        }

        new BounceAnimation(this, this.props.bounce).hookup();
    }


    public addChild(...child:PIXI.DisplayObject[]) {
        let result = super.addChild(...child);
        
        if(this.props.debug && this.indicator) {
            this.setChildIndex(this.indicator, this.children.length-1);
        }

        return result;
    }

    public addChildAt(child:any, index: number) {
        let result = super.addChildAt(child, index);

        if(this.props.debug && this.indicator) {
            this.setChildIndex(this.indicator, this.children.length-1);
        }

        return result;
    }

}