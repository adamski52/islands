import img from "../../../img/roots/root_4.png";
import Root from "./Root";

export default class Root4 extends Root {
    constructor() {
        let width = 130;

        super({
            img,
            width,
            height: 210
        });

        this.sprite.anchor.x = 116/width;
    }
}