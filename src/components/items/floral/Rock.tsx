import IBaseItemProps from "../../../interfaces/BaseItemProps";
import RandomService from "../../../services/RandomService";
import BaseItem from "../BaseItem";

export default class Rock extends BaseItem {
    constructor(props:IBaseItemProps) {
        super({
            ...props,
            debug: {
                indicatorColor: 0x927d18
            },
            flip: true
        });

        this.sprite.anchor.x = .5
        this.sprite.anchor.y = 1;

        let scale = RandomService.getRandomBetween(8, 12) / 10;
        this.scale.x *= scale;
        this.scale.y *= scale;
    }
}