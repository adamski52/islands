import img from "../../../img/trees/tree_trunk_2.png";
import TreeTrunk from "./TreeTrunk";

export default class TreeTrunk2 extends TreeTrunk {
    constructor() {
        let width = 128;
        
        super({
            img,
            width,
            height: 219
        });

        this.sprite.anchor.x = 63/width;
    }
}