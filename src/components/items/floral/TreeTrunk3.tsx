import img from "../../../img/trees/tree_trunk_3.png";
import TreeTrunk from "./TreeTrunk";

export default class TreeTrunk3 extends TreeTrunk {
    constructor() {
        let width = 183;
        
        super({
            img,
            width,
            height: 197
        });

        this.sprite.anchor.x = 88/width;
    }
}