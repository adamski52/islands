import img from "../../../img/rocks/rock_4.png";
import Rock from "./Rock";

export default class Rock4 extends Rock {
    constructor() {
        super({
            img,
            width: 89,
            height: 51
        });
    }
}