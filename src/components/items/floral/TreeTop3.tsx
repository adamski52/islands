import img from "../../../img/trees/tree_top_3.png";
import TreeTop from "./TreeTop";

export default class TreeTop3 extends TreeTop {
    constructor() {
        super({
            img,
            width: 328,
            height: 226
        });
    }
}