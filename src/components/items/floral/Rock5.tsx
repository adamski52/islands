import img from "../../../img/rocks/rock_5.png";
import Rock from "./Rock";

export default class Rock5 extends Rock {
    constructor() {
        super({
            img,
            width: 256,
            height: 68
        });
    }
}