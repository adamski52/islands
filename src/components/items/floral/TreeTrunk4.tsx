import img from "../../../img/trees/tree_trunk_4.png";
import TreeTrunk from "./TreeTrunk";

export default class TreeTrunk4 extends TreeTrunk {
    constructor() {
        let width = 174;

        super({
            img,
            width,
            height: 205
        });

        this.sprite.anchor.x = 113/width;
    }
}