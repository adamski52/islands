import img from "../../../img/trees/tree_top_2.png";
import TreeTop from "./TreeTop";

export default class TreeTop2 extends TreeTop {
    constructor() {
        super({
            img,
            width: 424,
            height: 266
        });
    }
}