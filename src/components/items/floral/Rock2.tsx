import img from "../../../img/rocks/rock_2.png";
import Rock from "./Rock";

export default class Rock2 extends Rock {
    constructor() {
        super({
            img,
            width: 102,
            height: 146
        });
    }
}