import img from "../../../img/vines/long_vine.png";
import Vine from "./Vine";

export default class Vine3 extends Vine {
    constructor() {
        super({
            img,
            width: 58,
            height: 782
        });
    }
}