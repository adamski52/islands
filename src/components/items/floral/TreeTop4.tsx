import img from "../../../img/trees/tree_top_4.png";
import TreeTop from "./TreeTop";

export default class TreeTop4 extends TreeTop {
    constructor() {
        super({
            img,
            width: 488,
            height: 322
        });
    }
}