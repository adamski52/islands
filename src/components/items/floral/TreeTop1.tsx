import img from "../../../img/trees/tree_top_1.png";
import TreeTop from "./TreeTop";

export default class TreeTop1 extends TreeTop {
    constructor() {
        super({
            img,
            width: 526,
            height: 292
        });
    }
}