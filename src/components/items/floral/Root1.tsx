import img from "../../../img/roots/root_1.png";
import Root from "./Root";

export default class Root1 extends Root {
    constructor() {
        let width = 179;
        
        super({
            img,
            width,
            height: 230
        });

        this.sprite.anchor.x = 152/width;

    }
}