import BaseItem from "../BaseItem";
import IBaseItemProps from "../../../interfaces/BaseItemProps";

export default class TreeTop extends BaseItem {
    constructor(props:IBaseItemProps) {
        super({
            debug: {
                indicatorColor: 0xff00ff
            },
            flip: true,
            ...props
        });

        this.sprite.anchor.x = .5
        this.sprite.anchor.y = 1;
    }
}