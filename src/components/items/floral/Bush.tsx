import BaseVariantItem from "../BaseVariantItem";
import IVariantProps from "../../../interfaces/VariantProps";
import RandomService from "../../../services/RandomService";

export default class Bush extends BaseVariantItem {
    constructor(props:IVariantProps) {
        super({
            ...props,
            debug: {
                indicatorColor: 0xffff00
            },
            flip: true,
            scale: {
                min: .9,
                max: 1.3
            }
        });

        this.sprite.anchor.x = .5
        this.sprite.anchor.y = 1;

        let scale = RandomService.getRandomBetween(8, 12) / 10;
        this.scale.x *= scale;
        this.scale.y *= scale;
    }
}