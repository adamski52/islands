import img from "../../../img/roots/root_2.png";
import Root from "./Root";

export default class Root2 extends Root {
    constructor() {
        let width = 95;
        
        super({
            img,
            width,
            height: 268
        });

        this.sprite.anchor.x = 78/width;
    }
}