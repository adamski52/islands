import img from "../../../img/trees/tree_trunk_1.png";
import TreeTrunk from "./TreeTrunk";

export default class TreeTrunk1 extends TreeTrunk {
    constructor() {
        let width = 313;
        
        super({
            img,
            width,
            height: 287
        });

        this.sprite.anchor.x = 231/width;
    }
}