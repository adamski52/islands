import img from "../../../img/trees/tree_top_5.png";
import TreeTop from "./TreeTop";

export default class TreeTop5 extends TreeTop {
    constructor() {
        super({
            img,
            width: 356,
            height: 224
        });
    }
}