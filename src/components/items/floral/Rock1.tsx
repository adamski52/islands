import img from "../../../img/rocks/rock_1.png";
import Rock from "./Rock";

export default class Rock1 extends Rock {
    constructor() {
        super({
            img,
            width: 216,
            height: 63,
            scale: {
                min: .9,
                max: 1.3
            }
        });
    }
}