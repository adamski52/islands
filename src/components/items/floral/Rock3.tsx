import img from "../../../img/rocks/rock_3.png";
import Rock from "./Rock";

export default class Rock3 extends Rock {
    constructor() {
        super({
            img,
            width: 140,
            height: 74
        });
    }
}