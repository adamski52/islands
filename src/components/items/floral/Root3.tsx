import img from "../../../img/roots/root_3.png";
import Root from "./Root";

export default class Root3 extends Root {
    constructor() {
        let width = 164;
        
        super({
            img,
            width,
            height: 126
        });

        this.sprite.anchor.x = 22/width;
    }
}