import BaseItem from "../BaseItem";
import IBaseItemProps from "../../../interfaces/BaseItemProps";
import { EasingTypes } from "../../../interfaces/EasingTypes";

export default class Vine extends BaseItem {
    constructor(props:IBaseItemProps) {
        super({
            ...props,
            flip: true,
            scale: {
                min: .9,
                max: 1.3
            },
            swing: {
                amount: {
                    atLeast: 1,
                    range: 5
                },
                durationPerAmount: 500,
                easeType: EasingTypes.EASE_IN_OUT_SINE
            }
        });
    }
}