import img from "../../../img/roots/root_5.png";
import Root from "./Root";

export default class Root5 extends Root {
    constructor() {
        let width = 130;

        super({
            img,
            width,
            height: 197,
        });

        this.sprite.anchor.x = 42/width;
    }
}