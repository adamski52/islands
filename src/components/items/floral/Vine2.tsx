import img from "../../../img/vines/medium_vine.png";
import Vine from "./Vine";

export default class Vine2 extends Vine {
    constructor() {
        super({
            img,
            width: 58,
            height: 357
        });
    }
}