import img from "../../../img/vines/short_vine.png";
import Vine from "./Vine";

export default class Vine1 extends Vine {
    constructor() {
        super({
            img,
            width: 59,
            height: 233
        });
    }
}