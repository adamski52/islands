import IBaseItemProps from "../../../interfaces/BaseItemProps";
import RandomService from "../../../services/RandomService";
import BaseItem from "../BaseItem";

export default class Root extends BaseItem {
    constructor(props:IBaseItemProps) {
        super({
            ...props,
            debug: {
                indicatorColor: 0xd07eba
            },
            flip: true,
            scale: {
                min: .9,
                max: 1.3
            }
        });

        this.sprite.anchor.x = .5;
        this.sprite.anchor.y = 0;

        let scale = RandomService.getRandomBetween(8, 12) / 10;
        this.scale.x *= scale;
        this.scale.y *= scale;
    }
}