import BaseItem from "../BaseItem";
import IBaseItemProps from "../../../interfaces/BaseItemProps";

export default class TreeTrunk extends BaseItem {
    constructor(props:IBaseItemProps) {
        super({
            debug: {
                indicatorColor: 0xf79c2c
            },
            flip: true,
            ...props
        });

        this.sprite.anchor.x = .5
        this.sprite.anchor.y = 1;
    }
}