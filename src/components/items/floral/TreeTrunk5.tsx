import img from "../../../img/trees/tree_trunk_5.png";
import TreeTrunk from "./TreeTrunk";

export default class TreeTrunk5 extends TreeTrunk {
    constructor() {
        let width = 300;

        super({
            img,
            width,
            height: 319
        });

        this.sprite.anchor.x = 195/width;
    }
}