import darkImg from "../../../img/bushes_grasses/dark_green/dark_green_bush_07.png";
import darkerImg from "../../../img/bushes_grasses/olive_green/olive_green_bush_07.png";
import lighterImg from "../../../img/bushes_grasses/light_green/light_green_bush_07.png";
import lightImg from "../../../img/bushes_grasses/island_grass_green/island_grass_green_07.png";
import Bush from "./Bush";

export default class Bush7 extends Bush {
    constructor() {
        super({
            variants: [darkImg, darkerImg, lightImg, lighterImg],
            width: 88,
            height: 51
        });
    }
}