import ItemContainer from "./ItemContainer";
import RandomService from "../../services/RandomService";
import BaseItem from "./BaseItem";
import TreeTop2 from "./floral/TreeTop2";
import TreeTop3 from "./floral/TreeTop3";
import TreeTop4 from "./floral/TreeTop4";
import TreeTop5 from "./floral/TreeTop5";
import TreeTrunk2 from "./floral/TreeTrunk2";
import TreeTrunk3 from "./floral/TreeTrunk3";
import TreeTrunk4 from "./floral/TreeTrunk4";
import TreeTrunk5 from "./floral/TreeTrunk5";
import TreeTop1 from "./floral/TreeTop1";
import TreeTrunk1 from "./floral/TreeTrunk1";
// import { EasingTypes } from "../../interfaces/EasingTypes";

export default class ModularTree extends ItemContainer {
    private canopy:BaseItem;
    private trunk:BaseItem;

    constructor() {
        super({
            // bounce: {
            //     amount: {
            //         atLeast: 1,
            //         range: .2
            //     },
            //     durationPerAmount: 10000,
            //     easeType: EasingTypes.LINEAR
            // },
            scale: {
                min: .9,
                max: 1.3
            }
        });

        this.trunk = new (RandomService.getRandomFromSet([TreeTrunk1, TreeTrunk2, TreeTrunk3, TreeTrunk4, TreeTrunk5]))();
        this.trunk.x = 0;
        this.trunk.y = -20;
        
        this.canopy = new (RandomService.getRandomFromSet([TreeTop1, TreeTop2, TreeTop3, TreeTop4, TreeTop5]))();
        this.canopy.x = 0;
        this.canopy.y = -170;
        
        this.addChild(this.trunk);
        this.addChild(this.canopy);
    }
}