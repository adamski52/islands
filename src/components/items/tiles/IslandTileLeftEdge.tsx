import BaseItem from "../BaseItem";
import img from "../../../img/island_tiles/left_edge.png";

export default class IslandTileLeftEdge extends BaseItem {
    constructor() {
        super({
            img,
            width: 254,
            height: 254
        });
    }
}