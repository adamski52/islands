import BaseItem from "../BaseItem";
import img from "../../../img/island_tiles/right_edge.png";

export default class IslandTileRightEdge extends BaseItem {
    constructor() {
        super({
            img,
            width: 254,
            height: 254
        });
    }
}