import BaseItem from "../BaseItem";
import img from "../../../img/island_tiles/center.png";

export default class IslandTileCenter extends BaseItem {
    constructor() {
        super({
            img,
            width: 254,
            height: 254
        });
    }
}