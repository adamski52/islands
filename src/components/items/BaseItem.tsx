import * as PIXI from "pixi.js";
import IBaseItemProps from "../../interfaces/BaseItemProps";
import ItemContainer from "./ItemContainer";
// import RandomService from "../../services/RandomService";

export default abstract class BaseItem extends ItemContainer {
    protected image: PIXI.Texture;
    protected sprite: PIXI.Sprite;

    constructor(props: IBaseItemProps) {
        super(props);

        this.image = PIXI.Texture.from(props.img);

        this.sprite = new PIXI.Sprite(this.image);
        this.sprite.anchor.x = 0;
        this.sprite.anchor.y = 0;
        this.sprite.width = props.width;
        this.sprite.height = props.height;
        
        this.addChild(this.sprite);
    }

    public getDisplaySize() {
        return {
            width: this.sprite.width,
            height: this.sprite.height
        };
    }
}